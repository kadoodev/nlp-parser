#!/bin/bash
set -e

source config

rm -f it-output-dev-noproj.txt

declare -a ORACLES=('liblinear') #Da aggiungere perchè ci vuole tempo
declare -a NON_PROJECTIVE_ALGOS=('covnonproj' 'stackeager' 'stacklazy');
declare -A development_times

for algorithm in "${NON_PROJECTIVE_ALGOS[@]}"
do
	for oracle in "${ORACLES[@]}"
	do
		echo "Testing with DEVELOPMENT_DATASET with algorithm: $algorithm and oracle: $oracle"
			java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c ${IT_OUTPUT_NONPROJ_MODEL}-${PROJECTIVIZATION_TYPE}-${algorithm}-${oracle} -i ${IT_DEV_DS} -m parse -if ${DS_FORMAT} -o ${IT_OUTPUT_NONPROJ_MODEL}-${PROJECTIVIZATION_TYPE}-${algorithm}-${oracle}-dev-parsed >> it-output-dev-noproj.txt 2>&1
		
		result_milliseconds=$(cat it-output-dev-noproj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')
		
		development_times[$algorithm-$oracle]=$result_milliseconds
	done
done

echo "Testing (DEV) times" 
for key in "${!development_times[@]}"; do
	echo $key - "${development_times[$key]} ms"; 
done
