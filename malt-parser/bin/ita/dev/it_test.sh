set -e

source config

rm -f it-output-test-proj.txt it-output-test-noproj.txt

##Projectivize BASELINE
java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c ${IT_OUTPUT_PROJECTIVIZATION_MODEL} -m proj -i ${IT_TEST_DS} -o ${PROJECTIVIZED_DATASET_NAME}-test -pp ${PROJECTIVIZATION_TYPE} -if ${DS_FORMAT}

#PROJ
java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c pproj-it-baseline-stackproj-liblinear.mco -i ${PROJECTIVIZED_DATASET_NAME}-test -m parse -if ${DS_FORMAT} -o pproj-it-baseline-stackproj-liblinear-test-parsed >> it-output-test-proj.txt 2>&1

result_milliseconds=$(cat it-output-test-proj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')

echo "Test time: $result_milliseconds " #mettere algoritmo qui

java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c pproj-it-baseline-nivrestandard-liblinear.mco -i ${PROJECTIVIZED_DATASET_NAME}-test -m parse -if ${DS_FORMAT} -o pproj-it-baseline-nivrestandard-liblinear-test-parsed >> it-output-test-proj.txt 2>&1

result_milliseconds=$(cat it-output-test-proj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')

echo "Test time: $result_milliseconds " #mettere algoritmo qui

#NON-PROJ
java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c nopproj-it-baseline-stacklazy-liblinear.mco -i ${IT_TEST_DS} -m parse -if ${DS_FORMAT} -o nopproj-it-baseline-stacklazy-liblinear-test-parsed >> it-output-test-noproj.txt 2>&1

result_milliseconds=$(cat it-output-test-noproj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')

echo "Test time: $result_milliseconds " #mettere algoritmo qui		

#Test time: 1158 pproj-it-baseline-stackproj-liblinear.mco
#Test time: 1085 pproj-it-baseline-nivrestandard-liblinear.mco
#Test time: 1143 nopproj-it-baseline-stacklazy-liblinear.mco