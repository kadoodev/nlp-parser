#!/bin/bash
set -e

source config

rm -f it-output-training-noproj.txt

declare -a ORACLES=('liblinear') #Da aggiungere perchè ci vuole tempo
declare -a NON_PROJECTIVE_ALGOS=('covnonproj' 'stackeager' 'stacklazy');
declare -A training_times

for algorithm in "${NON_PROJECTIVE_ALGOS[@]}"
do
	for oracle in "${ORACLES[@]}"
	do
		#timestamp_start=$(date +%s)
		echo "NON-(PROJ)raining with algorithm: $algorithm and oracle: $oracle"
		java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c ${IT_OUTPUT_NONPROJ_MODEL}-${PROJECTIVIZATION_TYPE}-${algorithm}-${oracle} -i ${IT_TRAIN_DS} -m learn -if ${DS_FORMAT} -a ${algorithm} -l ${oracle} >> it-output-training-noproj.txt 2>&1
		#timestamp_finish=$(date +%s)

		result_milliseconds=$(cat it-output-training-noproj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')
		
		#training_times[$algorithm-$oracle]=$(expr $timestamp_finish - $timestamp_start)
		training_times[$algorithm-$oracle]=$result_milliseconds
	done
done

echo "Training times" 
for key in "${!training_times[@]}"; do
	echo "$key - ${training_times[$key]} ms";
done
