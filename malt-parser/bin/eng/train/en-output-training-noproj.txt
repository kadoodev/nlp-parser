-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:19:34 CEST 2018
  Transition system    : Non-Projective
  Parser configuration : Covington with allow_root=true and allow_shift=false
  Oracle               : Covington
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/eng/train/nopproj-en-baseline-covnonproj-liblinear/conllu.xml
.          	      1	      0s	      6MB
.          	     10	      0s	     11MB
.          	    100	      0s	     57MB
..........	   1000	      2s	     96MB
..........	   2000	      4s	     84MB
..........	   3000	      5s	     98MB
..........	   4000	      6s	     50MB
..........	   5000	      7s	     76MB
..........	   6000	      8s	     52MB
..........	   7000	     10s	     17MB
..........	   8000	     11s	     51MB
..........	   9000	     13s	     47MB
..........	  10000	     14s	     37MB
..........	  11000	     15s	     67MB
..........	  12000	     16s	     38MB
.....      	  12543	     17s	    102MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:02:06 (126947 ms)
Finished: Sun Jun 17 16:21:41 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:21:41 CEST 2018
  Transition system    : Non-Projective
  Parser configuration : Stack
  Oracle               : Swap-Eager
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/eng/train/nopproj-en-baseline-stackeager-liblinear/conllu.xml
.          	      1	      0s	      5MB
.          	     10	      0s	      6MB
.          	    100	      0s	     26MB
..........	   1000	      0s	     61MB
..........	   2000	      1s	     10MB
..........	   3000	      1s	    102MB
..........	   4000	      2s	     83MB
..........	   5000	      2s	     37MB
..........	   6000	      2s	    159MB
..........	   7000	      3s	     65MB
..........	   8000	      3s	    197MB
..........	   9000	      4s	     67MB
..........	  10000	      4s	    202MB
..........	  11000	      4s	    322MB
..........	  12000	      5s	     40MB
.....      	  12543	      5s	    129MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:00:30 (30553 ms)
Finished: Sun Jun 17 16:22:12 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:22:12 CEST 2018
  Transition system    : Non-Projective
  Parser configuration : Stack
  Oracle               : Swap-Lazy
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/eng/train/nopproj-en-baseline-stacklazy-liblinear/conllu.xml
.          	      1	      0s	      5MB
.          	     10	      0s	      6MB
.          	    100	      0s	     26MB
..........	   1000	      1s	     58MB
..........	   2000	      1s	      7MB
..........	   3000	      1s	     97MB
..........	   4000	      2s	     79MB
..........	   5000	      2s	    168MB
..........	   6000	      2s	     27MB
..........	   7000	      3s	    195MB
..........	   8000	      3s	     54MB
..........	   9000	      4s	    183MB
..........	  10000	      4s	     57MB
..........	  11000	      4s	    172MB
..........	  12000	      5s	     61MB
.....      	  12543	      5s	    147MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:00:32 (32644 ms)
Finished: Sun Jun 17 16:22:45 CEST 2018
