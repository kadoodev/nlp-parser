-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:16:32 CEST 2018
  Transition system    : Arc-Eager
  Parser configuration : Nivre with allow_root=true, allow_reduce=false and enforce_tree=false
  Oracle               : Arc-Eager
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/eng/train/pproj-en-baseline-nivreeager-liblinear/conllu.xml
.          	      1	      0s	      5MB
.          	     10	      0s	      6MB
.          	    100	      0s	     26MB
..........	   1000	      0s	     58MB
..........	   2000	      1s	    136MB
..........	   3000	      1s	     93MB
..........	   4000	      2s	     73MB
..........	   5000	      2s	    163MB
..........	   6000	      2s	     21MB
..........	   7000	      3s	    188MB
..........	   8000	      3s	     60MB
..........	   9000	      3s	    192MB
..........	  10000	      4s	     82MB
..........	  11000	      4s	    196MB
..........	  12000	      5s	    102MB
.....      	  12543	      5s	    187MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:00:31 (31538 ms)
Finished: Sun Jun 17 16:17:04 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:17:04 CEST 2018
  Transition system    : Arc-Standard
  Parser configuration : Nivre with allow_root=true, allow_reduce=false and enforce_tree=false
  Oracle               : Arc-Standard
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/eng/train/pproj-en-baseline-nivrestandard-liblinear/conllu.xml
.          	      1	      0s	      5MB
.          	     10	      0s	      6MB
.          	    100	      0s	     25MB
..........	   1000	      0s	     52MB
..........	   2000	      1s	     62MB
..........	   3000	      1s	     85MB
..........	   4000	      2s	     60MB
..........	   5000	      2s	     11MB
..........	   6000	      2s	    132MB
..........	   7000	      3s	     29MB
..........	   8000	      3s	     27MB
..........	   9000	      3s	     34MB
..........	  10000	      4s	     47MB
..........	  11000	      4s	     42MB
..........	  12000	      4s	     65MB
.....      	  12543	      5s	     40MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:00:28 (28404 ms)
Finished: Sun Jun 17 16:17:33 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:17:33 CEST 2018
  Transition system    : Projective
  Parser configuration : Stack
  Oracle               : Projective
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/eng/train/pproj-en-baseline-stackproj-liblinear/conllu.xml
.          	      1	      0s	      5MB
.          	     10	      0s	      6MB
.          	    100	      0s	     26MB
..........	   1000	      0s	     55MB
..........	   2000	      1s	     66MB
..........	   3000	      1s	     15MB
..........	   4000	      2s	     52MB
..........	   5000	      2s	    143MB
..........	   6000	      2s	    116MB
..........	   7000	      3s	    141MB
..........	   8000	      3s	    129MB
..........	   9000	      3s	    126MB
..........	  10000	      4s	    130MB
..........	  11000	      4s	    117MB
..........	  12000	      5s	     15MB
.....      	  12543	      5s	     99MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:00:32 (32258 ms)
Finished: Sun Jun 17 16:18:05 CEST 2018
-----------------------------------------------------------------------------
                          MaltParser 1.9.2                             
-----------------------------------------------------------------------------
         MALT (Models and Algorithms for Language Technology) Group          
             Vaxjo University and Uppsala University                         
                             Sweden                                          
-----------------------------------------------------------------------------

Started: Sun Jun 17 16:18:05 CEST 2018
  Transition system    : Projective
  Parser configuration : Covington with allow_root=true and allow_shift=false
  Oracle               : Covington
  Data Format          : file:////home/eugenio/Documents/nlp-parser/malt-parser/bin/eng/train/pproj-en-baseline-covproj-liblinear/conllu.xml
.          	      1	      0s	      5MB
.          	     10	      0s	      7MB
.          	    100	      0s	     30MB
..........	   1000	      1s	     25MB
..........	   2000	      1s	     65MB
..........	   3000	      1s	     35MB
..........	   4000	      2s	     30MB
..........	   5000	      2s	    134MB
..........	   6000	      3s	     16MB
..........	   7000	      3s	     90MB
..........	   8000	      3s	    121MB
..........	   9000	      4s	     44MB
..........	  10000	      4s	     90MB
..........	  11000	      5s	    112MB
..........	  12000	      5s	     67MB
.....      	  12543	      5s	     69MB
Creating Liblinear model odm0.liblinear.moo
- Read all training instances.
- Train a parser model using LibLinear.
- Optimize the memory usage
- Save the Liblinear model odm0.liblinear.moo
Learning time: 00:00:39 (39801 ms)
Finished: Sun Jun 17 16:18:45 CEST 2018
