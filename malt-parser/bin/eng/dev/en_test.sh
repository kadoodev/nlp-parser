set -e

source config

rm -f en-output-test-proj.txt en-output-test-noproj.txt

##Projectivize BASELINE
java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c ${EN_OUTPUT_PROJECTIVIZATION_MODEL} -m proj -i ${EN_TEST_DS} -o ${PROJECTIVIZED_DATASET_NAME}-test -pp ${PROJECTIVIZATION_TYPE} -if ${DS_FORMAT}

#PROJ
java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c pproj-en-baseline-stackproj-liblinear.mco -i ${PROJECTIVIZED_DATASET_NAME}-test -m parse -if ${DS_FORMAT} -o pproj-en-baseline-stackproj-liblinear-test-parsed >> en-output-test-proj.txt 2>&1

result_milliseconds=$(cat en-output-test-proj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')

echo "Test time: $result_milliseconds " #mettere algoritmo qui

#NON-PROJ
java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c nopproj-en-baseline-stackeager-liblinear.mco -i ${EN_TEST_DS} -m parse -if ${DS_FORMAT} -o nopproj-en-baseline-stackeager-liblinear-test-parsed >> en-output-test-noproj.txt 2>&1

result_milliseconds=$(cat en-output-test-noproj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')

echo "Test time: $result_milliseconds " #mettere algoritmo qui

java ${JAVA_OPTS} -jar ${MALT_PARSER_PATH} -c nopproj-en-baseline-stacklazy-liblinear.mco -i ${EN_TEST_DS} -m parse -if ${DS_FORMAT} -o nopproj-en-baseline-stacklazy-liblinear-test-parsed >> en-output-test-noproj.txt 2>&1

result_milliseconds=$(cat en-output-test-noproj.txt | tail -n 2 | head -n 1 | awk '{ print substr($4,2) }')

echo "Test time: $result_milliseconds " #mettere algoritmo qui	

#Test time: 1851 pproj-en-baseline-stackproj-liblinear.mco
#Test time: 1935 nopproj-en-baseline-stackeager-liblinear.mco
#Test time: 1895	nopproj-en-baseline-stacklazy-liblinear.mco	